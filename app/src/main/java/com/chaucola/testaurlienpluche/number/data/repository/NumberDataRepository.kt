package com.chaucola.testaurlienpluche.number.data.repository

import com.chaucola.testaurlienpluche.number.data.api.NumberApi
import com.chaucola.testaurlienpluche.number.data.converter.NumberDetailEntityConverter
import com.chaucola.testaurlienpluche.number.data.converter.NumberEntityConverter
import com.chaucola.testaurlienpluche.number.domain.model.NumberDetailModel
import com.chaucola.testaurlienpluche.number.domain.model.NumberModel
import com.chaucola.testaurlienpluche.number.domain.repository.NumberRepository
import io.reactivex.Single
import javax.inject.Inject

class NumberDataRepository @Inject constructor(val numberApi: NumberApi) : NumberRepository {

    private val converter = NumberEntityConverter()
    private val converterDetail = NumberDetailEntityConverter()

    override fun getNumbers(): Single<List<NumberModel>> =
        numberApi.getNumbers().map { converter.convert(it) }

    override fun getNumberDetail(name: String): Single<NumberDetailModel> =
        numberApi.getNumberDetail(name).map { converterDetail.convert(it) }
}