package com.chaucola.testaurlienpluche.number.presentation.model

import com.chaucola.testaurlienpluche.common.view.StreamItem
import com.chaucola.testaurlienpluche.common.view.StreamItemType

data class NumberModelView(
    val name: String,
    val image: String
) : StreamItem {
    override fun getType(): StreamItemType {
        return StreamItemType.NUMBER
    }
}