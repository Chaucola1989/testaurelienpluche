package com.chaucola.testaurlienpluche.number.presentation.viewmodel

import com.chaucola.testaurlienpluche.common.core.*
import com.chaucola.testaurlienpluche.number.domain.usecase.NumberUseCase
import com.chaucola.testaurlienpluche.number.presentation.converter.NumberDetailModelConverter
import com.chaucola.testaurlienpluche.number.presentation.model.NumberDetailModelView
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class NumberDetailViewModel @Inject constructor(private val numberUseCase: NumberUseCase) :
    AppViewModel() {

    private val converter = NumberDetailModelConverter()

    val numberValue = ResourceLiveData<NumberDetailModelView>()

    fun getNumberDetail(name: String){
        numberUseCase.getNumberDetail(name)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { numberValue.loading() }
            .subscribeBy(
                onSuccess = { data ->
                    numberValue.success(converter.convert(data))
                },
                onError = {
                    var message = "Une erreur est apparue"
                    if (it.message != null) {
                        message = it.message!!
                    }
                    numberValue.error(message)
                }
            )
            .manageDisposable()
    }

}