package com.chaucola.testaurlienpluche.common.view

interface StreamItem {
    fun getType(): StreamItemType
}