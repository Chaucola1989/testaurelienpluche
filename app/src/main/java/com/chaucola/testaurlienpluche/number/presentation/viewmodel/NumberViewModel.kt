package com.chaucola.testaurlienpluche.number.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.chaucola.testaurlienpluche.common.core.AppViewModel
import com.chaucola.testaurlienpluche.number.domain.usecase.NumberUseCase
import com.chaucola.testaurlienpluche.number.presentation.converter.NumberModelConverter
import com.chaucola.testaurlienpluche.number.presentation.livedata.NumberLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class NumberViewModel @Inject constructor(private val numberUseCase: NumberUseCase) :
    AppViewModel() {

    private val converter = NumberModelConverter()

    private val _numberList = MutableLiveData<NumberLiveData>()
    val numberList: LiveData<NumberLiveData>
        get() = _numberList

    fun getNumbers() {
        numberUseCase.getNumbers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { _numberList.postValue(NumberLiveData(true)) }
            .subscribeBy(
                onSuccess = { data ->
                    _numberList.postValue(NumberLiveData(data = converter.convert(data)))
                },
                onError = {
                    var message = "Une erreur est apparue"
                    if (it.message != null) {
                        message = it.message!!
                    }
                    _numberList.postValue(NumberLiveData(error = message))
                }
            )
            .manageDisposable()
    }

}