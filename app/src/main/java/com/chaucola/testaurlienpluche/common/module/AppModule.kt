package com.chaucola.testaurlienpluche.common.module

import com.chaucola.testaurlienpluche.common.api.ConnectivityInterceptor
import com.chaucola.testaurlienpluche.common.constant.BASE_URL
import com.chaucola.testaurlienpluche.number.data.api.NumberApi
import com.chaucola.testaurlienpluche.number.data.repository.NumberDataRepository
import com.chaucola.testaurlienpluche.number.domain.repository.NumberRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier
import javax.inject.Singleton

private const val TIME_OUT_SECOND = 10

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun providePulseApi(
        @GlobalOkHttpClient okHttpClient: OkHttpClient
    ): NumberApi =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
            .create(NumberApi::class.java)

    @GlobalOkHttpClient
    @Provides
    fun provideGlobalInterceptorOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(TIME_OUT_SECOND.toLong(), TimeUnit.SECONDS)
        .readTimeout(TIME_OUT_SECOND.toLong(), TimeUnit.SECONDS)
        .addInterceptor(ConnectivityInterceptor())
        .build()

    @Provides
    @Singleton
    fun provideNumberRepository(
        watchListApi: NumberApi
    ): NumberRepository = NumberDataRepository(watchListApi)

}

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class GlobalOkHttpClient