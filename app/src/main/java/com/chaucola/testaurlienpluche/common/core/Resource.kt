package com.chaucola.testaurlienpluche.common.core

import androidx.lifecycle.MutableLiveData

sealed class Resource<T>
data class Success<T>(val data: T?) : Resource<T>()
data class Error<T>(val message: String?) : Resource<T>()
class Loading<T> : Resource<T>()

fun <T> MutableLiveData<Resource<T>>.success(data: T? = null) {
    value = Success(data)
}

fun <T> MutableLiveData<Resource<T>>.error(message: String? = null) {
    value = Error(message)
}

fun <T> MutableLiveData<Resource<T>>.loading() {
    value = Loading()
}

class ResourceLiveData<T> : MutableLiveData<Resource<T>>()


