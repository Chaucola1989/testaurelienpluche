package com.chaucola.testaurlienpluche.number.presentation.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.chaucola.testaurlienpluche.R
import com.chaucola.testaurlienpluche.number.presentation.fragment.NumberDetailFragment
import dagger.hilt.android.AndroidEntryPoint

private const val EXTRA_NAME = "extra_name"
private const val EXTRA_POSITION = "extra_position"

@AndroidEntryPoint
class NumberDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_container)

        var nameList = arrayListOf<String>()
        var position = 0

        intent.extras?.let {
            if (it.get(EXTRA_NAME) != null){
                nameList = it.getStringArrayList(EXTRA_NAME)!!
            }
            if (it.get(EXTRA_POSITION) != null){
                position = it.getInt(EXTRA_POSITION)
            }
        }

        supportFragmentManager.beginTransaction()
            .replace(R.id.container, NumberDetailFragment.newInstance(nameList, position))
            .commit()
    }

    companion object {
        fun buildIntent(context: Context, nameList: ArrayList<String>, position: Int): Intent {
            val intent = Intent(context, NumberDetailActivity::class.java)
            intent.putStringArrayListExtra(EXTRA_NAME, nameList)
            intent.putExtra(EXTRA_POSITION, position)
            return intent
        }
    }
}