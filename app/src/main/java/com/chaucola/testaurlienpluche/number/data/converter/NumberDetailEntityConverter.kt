package com.chaucola.testaurlienpluche.number.data.converter

import com.chaucola.testaurlienpluche.common.core.Converter
import com.chaucola.testaurlienpluche.number.data.model.NumberDetailEntity
import com.chaucola.testaurlienpluche.number.domain.model.NumberDetailModel

class NumberDetailEntityConverter : Converter<NumberDetailEntity, NumberDetailModel>() {

    override fun convert(obj: NumberDetailEntity): NumberDetailModel {
        var name = ""
        if (obj.name != null) {
            name = obj.name
        }
        var text = ""
        if (obj.text != null) {
            text = obj.text
        }
        var image = ""
        if (obj.image != null) {
            image = obj.image
        }
        return NumberDetailModel(name, text, image)
    }
}