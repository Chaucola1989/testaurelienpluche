package com.chaucola.testaurlienpluche.common.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.chaucola.testaurlienpluche.R

fun glide(context: Context?, url: String, imageView: ImageView) {
    context?.let {
        Glide.with(it)
            .load(url)
            .placeholder(R.drawable.ic_place_holder)
            .error(R.drawable.ic_place_holder)
            .into(imageView)
    }
}