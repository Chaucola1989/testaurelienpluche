package com.chaucola.testaurlienpluche.number.presentation.fragment;

import static com.chaucola.testaurlienpluche.common.utils.GlideUtilsKt.glide;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.chaucola.testaurlienpluche.R;
import com.chaucola.testaurlienpluche.common.core.Error;
import com.chaucola.testaurlienpluche.common.core.Loading;
import com.chaucola.testaurlienpluche.common.core.Resource;
import com.chaucola.testaurlienpluche.common.core.Success;
import com.chaucola.testaurlienpluche.number.presentation.model.NumberDetailModelView;
import com.chaucola.testaurlienpluche.number.presentation.viewmodel.NumberDetailViewModel;
import com.google.android.material.snackbar.Snackbar;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class NumberDetailPagerFragment extends Fragment {

    private static final String EXTRA_NAME_NUMBER = "EXTRA_NAME_NUMBER";

    private NumberDetailViewModel numberDetailViewModel;

    private LinearLayoutCompat container;
    private ProgressBar progressBar;
    private ImageView image;
    private TextView name;
    private TextView text;
    private View reloadView;

    private String nameExtra = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        numberDetailViewModel = new ViewModelProvider(this).get(NumberDetailViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_number_detail_pager, container, false);

        if (getArguments() != null) {
            nameExtra = getArguments().getString(EXTRA_NAME_NUMBER);
        }

        bindView(root);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        numberDetailViewModel.getNumberValue().observe(getViewLifecycleOwner(), this::handleValue);

        numberDetailViewModel.getNumberDetail(nameExtra);
    }

    private void bindView(View root) {
        container = root.findViewById(R.id.number_detail_container);
        progressBar = root.findViewById(R.id.number_detail_progress);
        image = root.findViewById(R.id.number_detail_image);
        name = root.findViewById(R.id.number_detail_name);
        text = root.findViewById(R.id.number_detail_text);
        reloadView = root.findViewById(R.id.number_detail_reload);

        reloadView.setOnClickListener(view -> {
            numberDetailViewModel.getNumberDetail(nameExtra);
            reloadView.setVisibility(View.GONE);
        });
    }


    private void handleValue(Resource<NumberDetailModelView> numberDetailModelViewResource) {
        if (numberDetailModelViewResource instanceof Loading) {
            setLoading(true);
        } else if (numberDetailModelViewResource instanceof Error) {
            setLoading(false);
            displayMessage(((Error<NumberDetailModelView>) numberDetailModelViewResource).getMessage());
        } else if (numberDetailModelViewResource instanceof Success) {
            setLoading(false);
            NumberDetailModelView numberDetailModelView = ((Success<NumberDetailModelView>) numberDetailModelViewResource).getData();
            if (numberDetailModelView != null) {
                name.setText(numberDetailModelView.getName());
                text.setText(numberDetailModelView.getText());
                glide(requireContext(), numberDetailModelView.getImage(), image);
            }
        }
    }

    private void setLoading(Boolean isLoading) {
        if (progressBar != null) {
            if (isLoading) {
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    private void displayMessage(String message) {
        if (container != null) {
            Snackbar.make(container, message, Snackbar.LENGTH_LONG).show();
        }
        if (message.equals(getString(R.string.error_no_internet))) {
            if (reloadView != null) {
                reloadView.setVisibility(View.VISIBLE);
            }
        }
    }

    public static NumberDetailPagerFragment newInstance(String name) {
        final Bundle args = new Bundle();
        args.putString(EXTRA_NAME_NUMBER, name);
        final NumberDetailPagerFragment fragment = new NumberDetailPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
