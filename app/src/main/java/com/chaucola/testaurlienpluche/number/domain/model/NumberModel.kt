package com.chaucola.testaurlienpluche.number.domain.model

data class NumberModel(
    val name: String,
    val image: String
)