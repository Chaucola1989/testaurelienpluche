package com.chaucola.testaurlienpluche.number.presentation.livedata

import com.chaucola.testaurlienpluche.number.presentation.model.NumberModelView

data class NumberLiveData(
    val isLoading: Boolean = false,
    val data: List<NumberModelView>? = null,
    val error: String = ""
)