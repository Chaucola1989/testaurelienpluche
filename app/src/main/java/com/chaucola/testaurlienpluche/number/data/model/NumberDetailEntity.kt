package com.chaucola.testaurlienpluche.number.data.model

import com.google.gson.annotations.SerializedName

data class NumberDetailEntity(
    @SerializedName("name")
    val name: String?,

    @SerializedName("text")
    val text: String?,

    @SerializedName("image")
    val image: String?
)