package com.chaucola.testaurlienpluche.number.data.api

import com.chaucola.testaurlienpluche.number.data.model.NumberDetailEntity
import com.chaucola.testaurlienpluche.number.data.model.NumberEntity
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NumberApi {

    @GET("test/json.php")
    fun getNumbers(): Single<List<NumberEntity>>

    @GET("test/json.php")
    fun getNumberDetail(@Query("name") name: String): Single<NumberDetailEntity>

}