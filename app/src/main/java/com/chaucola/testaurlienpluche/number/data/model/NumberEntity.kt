package com.chaucola.testaurlienpluche.number.data.model

import com.google.gson.annotations.SerializedName

data class NumberEntity(
    @SerializedName("name")
    val name: String?,

    @SerializedName("image")
    val image: String?
)