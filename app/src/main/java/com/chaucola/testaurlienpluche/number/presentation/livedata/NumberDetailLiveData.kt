package com.chaucola.testaurlienpluche.number.presentation.livedata

import com.chaucola.testaurlienpluche.number.presentation.model.NumberDetailModelView

data class NumberDetailLiveData(
    val isLoading: Boolean = false,
    val data: NumberDetailModelView? = null,
    val error: String = ""
)