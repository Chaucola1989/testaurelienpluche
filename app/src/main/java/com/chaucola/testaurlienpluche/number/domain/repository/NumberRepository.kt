package com.chaucola.testaurlienpluche.number.domain.repository

import com.chaucola.testaurlienpluche.number.domain.model.NumberDetailModel
import com.chaucola.testaurlienpluche.number.domain.model.NumberModel
import io.reactivex.Single

interface NumberRepository {
    fun getNumbers(): Single<List<NumberModel>>

    fun getNumberDetail(name: String): Single<NumberDetailModel>
}