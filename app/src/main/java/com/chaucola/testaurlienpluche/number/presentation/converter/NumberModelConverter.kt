package com.chaucola.testaurlienpluche.number.presentation.converter

import com.chaucola.testaurlienpluche.common.core.Converter
import com.chaucola.testaurlienpluche.number.domain.model.NumberModel
import com.chaucola.testaurlienpluche.number.presentation.model.NumberModelView

class NumberModelConverter : Converter<NumberModel, NumberModelView>() {

    override fun convert(obj: NumberModel): NumberModelView = NumberModelView(obj.name, obj.image)
}