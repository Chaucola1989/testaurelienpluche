package com.chaucola.testaurlienpluche.number.presentation.converter

import com.chaucola.testaurlienpluche.common.core.Converter
import com.chaucola.testaurlienpluche.number.domain.model.NumberDetailModel
import com.chaucola.testaurlienpluche.number.presentation.model.NumberDetailModelView

class NumberDetailModelConverter : Converter<NumberDetailModel, NumberDetailModelView>() {

    override fun convert(obj: NumberDetailModel): NumberDetailModelView =
        NumberDetailModelView(obj.name, obj.text, obj.image)
}