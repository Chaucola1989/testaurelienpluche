package com.chaucola.testaurlienpluche.number.data.converter

import com.chaucola.testaurlienpluche.common.core.Converter
import com.chaucola.testaurlienpluche.number.data.model.NumberEntity
import com.chaucola.testaurlienpluche.number.domain.model.NumberModel

class NumberEntityConverter : Converter<NumberEntity, NumberModel>() {

    override fun convert(obj: NumberEntity): NumberModel {
        var name = ""
        if (obj.name != null) {
            name = obj.name
        }
        var image = ""
        if (obj.image != null) {
            image = obj.image
        }

        return NumberModel(name, image)
    }
}