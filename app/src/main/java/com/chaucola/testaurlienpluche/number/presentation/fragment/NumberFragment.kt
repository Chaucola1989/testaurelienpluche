package com.chaucola.testaurlienpluche.number.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.chaucola.testaurlienpluche.R
import com.chaucola.testaurlienpluche.common.view.StreamItem
import com.chaucola.testaurlienpluche.common.view.StreamItemAdapter
import com.chaucola.testaurlienpluche.number.presentation.activity.NumberDetailActivity
import com.chaucola.testaurlienpluche.number.presentation.livedata.NumberLiveData
import com.chaucola.testaurlienpluche.number.presentation.model.NumberModelView
import com.chaucola.testaurlienpluche.number.presentation.viewmodel.NumberViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NumberFragment : Fragment(), StreamItemAdapter.Listener {

    private val numberViewModel: NumberViewModel by viewModels()

    private val adapter: StreamItemAdapter by lazy {
        StreamItemAdapter(this)
    }

    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var recyclerView: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var reloadView: View? = null
    private var currentPressed = -1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.layout_fragment_number, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindView(view)

        numberViewModel.numberList.observe(viewLifecycleOwner) { numberList ->
            handleNumberList(numberList)
        }

        if (savedInstanceState == null) {
            numberViewModel.getNumbers()
        }
    }

    override fun onStreamItemClick(streamItemList: List<StreamItem>, position: Int) {
        if (!resources.getBoolean(R.bool.isTablet)) {
            startActivity(
                NumberDetailActivity.buildIntent(
                    requireContext(),
                    getListNames(streamItemList),
                    position
                )
            )
        } else {
            if (currentPressed != -1) {
                adapter.updateDataList(currentPressed)
            }
            currentPressed = position
            val numberModelView: NumberModelView = streamItemList[position] as NumberModelView
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(
                    R.id.fragment_number_detail,
                    NumberDetailPagerFragment.newInstance(numberModelView.name)
                )
                .commit()
        }
    }

    private fun getListNames(streamItemList: List<StreamItem>): ArrayList<String> {
        val nameList = arrayListOf<String>()
        streamItemList.forEach { streamItem ->
            val numberModelView: NumberModelView = streamItem as NumberModelView
            nameList.add(numberModelView.name)
        }
        return nameList
    }

    private fun setLoading(isLoading: Boolean) {
        progressBar?.let {
            if (isLoading) {
                it.visibility = View.VISIBLE
            } else {
                it.visibility = View.GONE
            }
        }
        swipeRefreshLayout?.let {
            if (!isLoading) {
                it.isRefreshing = false
            }
        }
    }

    private fun displayMessage(message: String) {
        setLoading(false)
        swipeRefreshLayout?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_LONG).show()
        }
        if (message == getString(R.string.error_no_internet)) {
            reloadView?.visibility = View.VISIBLE
        }
    }

    private fun bindView(view: View) {
        progressBar = view.findViewById(R.id.fragment_number_progress)
        recyclerView = view.findViewById(R.id.fragment_number_recycler_view)
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout)
        reloadView = view.findViewById(R.id.fragment_number_reload)
        with(recyclerView!!) {
            this.adapter = this@NumberFragment.adapter
            this.layoutManager = LinearLayoutManager(requireContext())
            val divider = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
            ContextCompat.getDrawable(context, R.drawable.div_vertical)?.run {
                divider.setDrawable(this)
            }
            addItemDecoration(divider)
        }

        swipeRefreshLayout!!.setOnRefreshListener {
            numberViewModel.getNumbers()
        }

        reloadView?.setOnClickListener {
            numberViewModel.getNumbers()
            it.visibility = View.GONE
        }
    }

    private fun handleNumberList(numberList: NumberLiveData) {
        when {
            numberList.isLoading -> setLoading(numberList.isLoading)
            numberList.error.isNotEmpty() -> displayMessage(numberList.error)
            numberList.data != null -> {
                setLoading(false)
                adapter.setDataList(numberList.data)
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = NumberFragment()
    }
}