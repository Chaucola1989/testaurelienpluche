package com.chaucola.testaurlienpluche.number.presentation.model

data class NumberDetailModelView(
    val name: String,
    val text: String,
    val image: String
)