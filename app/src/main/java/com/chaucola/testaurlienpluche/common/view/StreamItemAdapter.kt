package com.chaucola.testaurlienpluche.common.view

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chaucola.testaurlienpluche.number.presentation.item.NumberViewHolder

class StreamItemAdapter(private val listener: Listener?) :
    RecyclerView.Adapter<StreamItemViewHolder>() {

    private var dataList: List<StreamItem> = emptyList()

    interface Listener {
        fun onStreamItemClick(streamItemList: List<StreamItem>, position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StreamItemViewHolder {
        return when (StreamItemType.values()[viewType]) {
            StreamItemType.NUMBER -> StreamItemViewHolder(NumberViewHolder(parent.context))
        }
    }

    override fun onBindViewHolder(holder: StreamItemViewHolder, position: Int) {
        (holder.itemView as StreamItemBaseView).setStreamItem(dataList[position])
        holder.itemView.setOnClickListener {
            (holder.itemView as NumberViewHolder).setColorPressed()
            listener?.onStreamItemClick(dataList, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (dataList.isNotEmpty()) dataList[position].getType().ordinal else 0
    }

    override fun getItemCount(): Int {
        return if (dataList.isEmpty()) 0 else dataList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDataList(list: List<StreamItem>) {
        this.dataList = list
        notifyDataSetChanged()
    }

    fun updateDataList(position: Int) {
        notifyItemChanged(position)
    }

}

class StreamItemViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView)