package com.chaucola.testaurlienpluche.common.core

abstract class Converter<E, M> {
    abstract fun convert(obj: E): M

    fun convert(collection: Collection<E>): List<M> =
        ArrayList(collection.map { convert(it) })

    fun convertOrNull(obj: E?): M? = obj?.let { convert(it) }

    fun convertOrNull(collection: Collection<E>?): List<M>? =
        collection?.let { collection.map { convert(it) } }
}