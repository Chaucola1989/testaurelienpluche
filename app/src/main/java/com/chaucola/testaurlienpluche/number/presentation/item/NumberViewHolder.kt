package com.chaucola.testaurlienpluche.number.presentation.item

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import com.chaucola.testaurlienpluche.R
import com.chaucola.testaurlienpluche.common.utils.glide
import com.chaucola.testaurlienpluche.common.view.StreamItem
import com.chaucola.testaurlienpluche.common.view.StreamItemBaseView
import com.chaucola.testaurlienpluche.number.presentation.model.NumberModelView

class NumberViewHolder : FrameLayout, StreamItemBaseView {

    private var container: LinearLayoutCompat? = null
    private var nameTextView: TextView? = null
    private var imageView: ImageView? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    init {
        val inflater = LayoutInflater.from(context).inflate(R.layout.item_number, this, true)
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        )
        layoutParams = params

        container = inflater.findViewById(R.id.item_number_container)
        nameTextView = inflater.findViewById(R.id.item_number_name)
        imageView = inflater.findViewById(R.id.item_number_image)
    }

    override fun setStreamItem(streamItem: StreamItem) {
        if (context.resources.getBoolean(R.bool.isTablet)) {
            container?.background =
                ContextCompat.getDrawable(context, R.drawable.color_background_item_number)
        }
        val numberModelView: NumberModelView = streamItem as NumberModelView
        nameTextView?.let {
            it.text = numberModelView.name
        }
        imageView?.let {
            glide(context, numberModelView.image, it)
        }
    }

    fun setColorPressed() {
        if (context.resources.getBoolean(R.bool.isTablet)) {
            container?.background =
                ContextCompat.getDrawable(context, R.drawable.color_background_item_number_pressed)
        }
    }
}