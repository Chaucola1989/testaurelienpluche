package com.chaucola.testaurlienpluche.number.domain.usecase

import com.chaucola.testaurlienpluche.number.domain.repository.NumberRepository
import javax.inject.Inject

class NumberUseCase @Inject constructor(val numberRepository: NumberRepository) {

    fun getNumbers() = numberRepository.getNumbers()

    fun getNumberDetail(name: String) = numberRepository.getNumberDetail(name)
}