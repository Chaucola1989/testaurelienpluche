package com.chaucola.testaurlienpluche.common.core

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class AppViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        with(compositeDisposable) {
            dispose()
            clear()
        }
    }

    fun Disposable.manageDisposable(): Disposable = apply { compositeDisposable.add(this) }
}