package com.chaucola.testaurlienpluche.number.presentation.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.chaucola.testaurlienpluche.number.presentation.fragment.NumberDetailPagerFragment

class NumberDetailPagerAdapter(fm: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fm, lifecycle) {

    private var data = emptyList<String>()

    override fun createFragment(position: Int): Fragment {
        var name = ""
        if (position < data.size) {
            name = data[position]
        }
        return NumberDetailPagerFragment.newInstance(name)
    }

    override fun getItemCount(): Int = data.size

    fun setData(newData: List<String>) {
        this.data = newData
    }
}