package com.chaucola.testaurlienpluche.number.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.chaucola.testaurlienpluche.R
import com.chaucola.testaurlienpluche.number.presentation.adapter.NumberDetailPagerAdapter
import dagger.hilt.android.AndroidEntryPoint

private const val EXTRA_NAME_LIST = "EXTRA_NAME_LIST"
private const val EXTRA_POSITION = "EXTRA_POSITION"

@AndroidEntryPoint
class NumberDetailFragment : Fragment() {

    private var nameList = arrayListOf<String>()
    private var position = 0

    private val pagerAdapter: NumberDetailPagerAdapter by lazy {
        NumberDetailPagerAdapter(childFragmentManager, lifecycle)
    }

    private var viewPager: ViewPager2? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_number_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindView(view)
    }

    private fun bindView(view: View) {
        viewPager = view.findViewById(R.id.pager)
        with(viewPager!!) {
            this.adapter = this@NumberDetailFragment.pagerAdapter
        }
        if (arguments != null) {
            nameList = requireArguments().getStringArrayList(EXTRA_NAME_LIST)!!
            position = requireArguments().getInt(EXTRA_POSITION)
        }
        pagerAdapter.setData(nameList)
        viewPager!!.currentItem = position
    }

    companion object {
        @JvmStatic
        fun newInstance(nameList: ArrayList<String>, position: Int) : NumberDetailFragment {
            val fragment = NumberDetailFragment()
            val args = Bundle()
            args.putStringArrayList(EXTRA_NAME_LIST, nameList)
            args.putInt(EXTRA_POSITION, position)
            fragment.arguments = args
            return fragment
        }
    }
}