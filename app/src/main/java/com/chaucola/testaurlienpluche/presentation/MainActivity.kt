package com.chaucola.testaurlienpluche.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.chaucola.testaurlienpluche.R
import com.chaucola.testaurlienpluche.common.api.WifiService
import com.chaucola.testaurlienpluche.number.presentation.fragment.NumberFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_container)

        WifiService.instance.initializeWithApplicationContext(this)

        supportFragmentManager.beginTransaction()
            .replace(R.id.container, NumberFragment.newInstance())
            .commit()
    }

}