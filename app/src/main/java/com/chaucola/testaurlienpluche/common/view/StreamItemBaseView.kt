package com.chaucola.testaurlienpluche.common.view

interface StreamItemBaseView {
    fun setStreamItem(streamItem: StreamItem)
}